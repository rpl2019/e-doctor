package app.andropath.com.edoctor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.widget.ImageView;
import android.widget.TextView;

public class detailrumahsakit extends AppCompatActivity {
    final String PREF_NIGHT_MODE = "NightMode";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.AppTheme);
            SharedPreferences Night = getSharedPreferences(PREF_NIGHT_MODE, Context.MODE_PRIVATE);
            if (Night.getBoolean(PREF_NIGHT_MODE, false)) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_rs);
        Intent intent = getIntent();
        rumahsakit rsItem = intent.getParcelableExtra("rumahsakit Item");

        int imageRes = rsItem.getFoto();
        String nama = rsItem.getName();
        String alamat = rsItem.getAlamat();
        String kota = rsItem.getKota();

        ImageView imageView = findViewById(R.id.drs_foto);
        imageView.setImageResource(imageRes);

        TextView textProfil1 = findViewById(R.id.drs_nama);
        textProfil1.setText(nama);

        TextView textProfil2 = findViewById(R.id.drs_alamat);
        textProfil2.setText(alamat);

        TextView textProfil3 = findViewById(R.id.drs_kota);
        textProfil3.setText(kota);
    }
}