package app.andropath.com.edoctor;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AdapterRumahSakit extends RecyclerView.Adapter<AdapterRumahSakit.ViewHolder> implements Filterable {
    private ArrayList<rumahsakit> mrumahsakit;
    private ArrayList<rumahsakit> rumahsakitfull;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(AdapterRumahSakit.OnItemClickListener listener) {
        mListener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageView;
        private TextView name;
        private TextView kota;

        public ViewHolder(View itemView, final AdapterRumahSakit.OnItemClickListener listener){
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.rsImage);
            name = (TextView) itemView.findViewById(R.id.titlers);
            kota = (TextView) itemView.findViewById(R.id.kotars);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public AdapterRumahSakit(ArrayList<rumahsakit> rumahsakit){

        this.mrumahsakit = rumahsakit;
        rumahsakitfull = new ArrayList<>(rumahsakit);
    }

    @Override
    public AdapterRumahSakit.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_daftar_rumahsakit, parent, false);
        ViewHolder dtk = new ViewHolder(v, mListener);
        return dtk;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        rumahsakit currentItem = mrumahsakit.get(position);

        holder.name.setText(currentItem.getName());
        holder.kota.setText(currentItem.getKota());
        holder.imageView.setImageResource(currentItem.getFoto());
    }

    @Override
    public int getItemCount() {
        return mrumahsakit.size();
    }

    @Override
    public Filter getFilter() {
        return rumahsakitFilter;
    }

    private Filter rumahsakitFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<rumahsakit> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(rumahsakitfull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (rumahsakit item : rumahsakitfull) {
                    if (item.getKota().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
                for (rumahsakit item : rumahsakitfull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mrumahsakit.clear();
            mrumahsakit.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };
}