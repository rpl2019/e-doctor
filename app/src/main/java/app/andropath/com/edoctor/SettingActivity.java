package app.andropath.com.edoctor;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import java.util.Locale;

public class SettingActivity extends AppCompatActivity {
    private Switch tema;
    private Button save;
    SharedPreferences Night;
    final String PREF_NIGHT_MODE = "NightMode";
    SharedPreferences.Editor editTheme;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.AppTheme);
            Night = getSharedPreferences(PREF_NIGHT_MODE, Context.MODE_PRIVATE);
            if (Night.getBoolean(PREF_NIGHT_MODE, false)) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
        }

        super.onCreate(savedInstanceState);
        loadlocale();
        setContentView(R.layout.activity_setting);

        Button changelang = findViewById(R.id.language_btn);
        tema = findViewById(R.id.tema);
        save = findViewById(R.id.save_conf);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveTheme();
            }
        });
        changelang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangeLanguageDialog();
            }
        });

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            tema.setChecked(true);
        }

        tema.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
            }
        });

    }


    private void showChangeLanguageDialog() {
        final String[] listitems = {"English", "Indonesia"};
        AlertDialog.Builder nBuilder = new AlertDialog.Builder(SettingActivity.this);
        nBuilder.setSingleChoiceItems(listitems, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if (i == 0){
                    setLocale("en");
                    recreate();
                }
                if (i == 1){
                    setLocale("in");
                    recreate();
                }
                dialog.dismiss();
            }
        });

        AlertDialog nDialog = nBuilder.create();
        nDialog.show();
    }

    private void setLocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("My lang", lang);
        editor.apply();

    }

    private void saveTheme() {
        if (tema.isChecked()){
            editTheme = Night.edit();
            editTheme.putBoolean(PREF_NIGHT_MODE, true);
            editTheme.apply();
        }else{
            editTheme = Night.edit();
            editTheme.putBoolean(PREF_NIGHT_MODE, false);
            editTheme.apply();
        }

    }

    public void loadlocale(){
        SharedPreferences pref = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = pref.getString("My Lang", "");
        setLocale(language);
    }

}



