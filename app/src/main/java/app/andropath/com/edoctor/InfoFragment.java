package app.andropath.com.edoctor;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class InfoFragment extends Fragment {
    final String PREF_NIGHT_MODE = "NightMode";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getContext().setTheme(R.style.DarkTheme);
        } else {
            getContext().setTheme(R.style.AppTheme);
            SharedPreferences Night = getContext().getSharedPreferences(PREF_NIGHT_MODE, Context.MODE_PRIVATE);
            if (Night.getBoolean(PREF_NIGHT_MODE, false)) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
        }
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        WebView webView= (WebView)view.findViewById(R.id.webView1);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("https://doktersehat.com/");
        return view;
    }
}