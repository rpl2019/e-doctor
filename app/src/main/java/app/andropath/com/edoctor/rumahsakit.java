package app.andropath.com.edoctor;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class rumahsakit implements Parcelable {
    private String rumahsakit;
    private String alamat;
    private String name;
    private String kota;
    private int foto;



    public rumahsakit(String name, String alamat, String kota, Integer foto) {
        rumahsakit = rumahsakit;
        this.name = name;
        this.kota = kota;
        this.alamat = alamat;
        this.foto = foto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota){this.kota = kota;}

    public int getFoto() {return foto;}

    public void setFoto(int foto){this.foto = foto;}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.rumahsakit);
        dest.writeString(this.alamat);
        dest.writeString(this.name);
        dest.writeString(this.kota);
        dest.writeInt(this.foto);
    }

    protected rumahsakit(Parcel in) {
        this.rumahsakit = in.readString();
        this.alamat = in.readString();
        this.name = in.readString();
        this.kota = in.readString();
        this.foto = in.readInt();
    }

    public static final Creator<rumahsakit> CREATOR = new Creator<rumahsakit>() {
        @Override
        public rumahsakit createFromParcel(Parcel source) {
            return new rumahsakit(source);
        }

        @Override
        public rumahsakit[] newArray(int size) {
            return new rumahsakit[size];
        }
    };
}