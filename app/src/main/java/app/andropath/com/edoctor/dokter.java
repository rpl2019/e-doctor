package app.andropath.com.edoctor;


import android.os.Parcel;
import android.os.Parcelable;

public class dokter implements Parcelable {
    private String dokter;
    private String pendidikan;
    private String praktek;
    private String str;
    private String name;
    private String spesialis;
    private String kota;
    private String hari;
    private String jam;
    private int foto;


    public dokter(String name, String spesialis, String kota, Integer foto, String pendidikan, String praktek, String str, String hari, String jam){
        dokter=dokter;
        this.name=name;
        this.kota=kota;
        this.spesialis=spesialis;
        this.foto=foto;
        this.pendidikan=pendidikan;
        this.praktek=praktek;
        this.str=str;
        this.hari=hari;
        this.jam=jam;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpesialis() {
        return spesialis;
    }

    public void setSpesialis(String spesialis) {
        this.spesialis = spesialis;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota){this.kota = kota;}

    public int getFoto(){ return foto;}

    public String getPendidikan() {
        return pendidikan;
    }

    public void setPendidikan(String pendidikan) {
        this.pendidikan = pendidikan;
    }

    public String getPraktek() {
        return praktek;
    }

    public void setPraktek(String praktek) {
        this.praktek = praktek;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) { this.str = str; }

    public String getHari() { return hari; }

    public void setHari(String hari) { this.hari = hari; }

    public String getJam() { return jam; }

    public void setJam(String jam) { this.jam = jam; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.dokter);
        dest.writeString(this.pendidikan);
        dest.writeString(this.praktek);
        dest.writeString(this.str);
        dest.writeString(this.name);
        dest.writeString(this.spesialis);
        dest.writeString(this.kota);
        dest.writeString(this.hari);
        dest.writeString(this.jam);
        dest.writeInt(this.foto);
    }

    protected dokter(Parcel in) {
        this.dokter = in.readString();
        this.pendidikan = in.readString();
        this.praktek = in.readString();
        this.str = in.readString();
        this.name = in.readString();
        this.spesialis = in.readString();
        this.kota = in.readString();
        this.hari = in.readString();
        this.jam = in.readString();
        this.foto = in.readInt();
    }

    public static final Creator<dokter> CREATOR = new Creator<dokter>() {
        @Override
        public dokter createFromParcel(Parcel source) {
            return new dokter(source);
        }

        @Override
        public dokter[] newArray(int size) {
            return new dokter[size];
        }
    };
}