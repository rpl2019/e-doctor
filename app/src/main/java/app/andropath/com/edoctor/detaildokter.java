package app.andropath.com.edoctor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class detaildokter extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    final String PREF_NIGHT_MODE = "NightMode";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.AppTheme);
            SharedPreferences Night = getSharedPreferences(PREF_NIGHT_MODE, Context.MODE_PRIVATE);
            if (Night.getBoolean(PREF_NIGHT_MODE, false)) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detaildokter);
        // kita set default nya Home Fragment
        //loadFragment(new ProfilDokterFragment());
        // inisialisasi BottomNavigaionView
        // beri listener pada saat item/menu bottomnavigation terpilih
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.bn_dokter);
        navigation.setOnNavigationItemSelectedListener(this);
        Intent intent = getIntent();
        dokter dokterItem = intent.getParcelableExtra("dokter Item");

        int imageRes = dokterItem.getFoto();
        String nama = dokterItem.getName();
        String spesialis = dokterItem.getSpesialis();
        String str = dokterItem.getStr();
        String pddkn = dokterItem.getPendidikan();
        String praktek = dokterItem.getPraktek();
        String kota = dokterItem.getKota();
        String hari = dokterItem.getHari();
        String jam = dokterItem.getJam();


        ImageView imageView = findViewById(R.id.dt_foto);
        imageView.setImageResource(imageRes);

        TextView textProfil1 = findViewById(R.id.dt_nama);
        textProfil1.setText(nama);

        TextView textProfil2 = findViewById(R.id.dt_spesialis);
        textProfil2.setText(spesialis);

        TextView textProfil3 = findViewById(R.id.dt_str);
        textProfil3.setText(str);

        TextView textProfil4 = findViewById(R.id.dt_pendidikan);
        textProfil4.setText(pddkn);

        TextView textProfil5 = findViewById(R.id.dt_rs);
        textProfil5.setText(praktek);

        TextView textProfil6 = findViewById(R.id.dt_kota);
        textProfil6.setText(kota);

        TextView textjadwal1 = findViewById(R.id.dt_hari);
        textjadwal1.setText(hari);

        TextView textjadwal2 = findViewById(R.id.dt_jam);
        textjadwal2.setText(jam);
    }


        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.profil_menu:
                    ((LinearLayout) findViewById(R.id.jadwal)).setVisibility(View.GONE);
                    ((LinearLayout) findViewById(R.id.profil)).setVisibility(View.VISIBLE);
                    return true;
                case R.id.jadwal_menu:
                    ((LinearLayout) findViewById(R.id.profil)).setVisibility(View.GONE);
                    ((LinearLayout) findViewById(R.id.jadwal)).setVisibility(View.VISIBLE);
                    return true;
            }
            return false;
        }

}