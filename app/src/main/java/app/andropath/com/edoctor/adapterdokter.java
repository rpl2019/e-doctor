package app.andropath.com.edoctor;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class adapterdokter extends RecyclerView.Adapter<adapterdokter.ViewHolder> implements Filterable {
    private ArrayList<dokter> mdokter;
    private ArrayList<dokter> dokterFull;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageView;
        private TextView name;
        private TextView spesialis, kota;

        public ViewHolder(View itemView, final OnItemClickListener listener){
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            name = (TextView) itemView.findViewById(R.id.name);
            spesialis = (TextView) itemView.findViewById(R.id.spesialis);
            kota = (TextView) itemView.findViewById(R.id.kota);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public adapterdokter(ArrayList<dokter> dokter){

        this.mdokter = dokter;
        dokterFull = new ArrayList<>(dokter);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        ViewHolder dtk = new ViewHolder(v, mListener);
        return dtk;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        dokter currentItem = mdokter.get(position);

        holder.name.setText(currentItem.getName());
        holder.spesialis.setText(currentItem.getSpesialis());
        holder.kota.setText(currentItem.getKota());
        holder.imageView.setImageResource(currentItem.getFoto());
    }

    @Override
    public int getItemCount() {
        return mdokter.size();
    }

    @Override
    public Filter getFilter() {
        return dokterFilter;
    }

    private Filter dokterFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<dokter> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(dokterFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (dokter item : dokterFull) {
                    if (item.getKota().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
                for (dokter item : dokterFull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
                for (dokter item : dokterFull) {
                    if (item.getSpesialis().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mdokter.clear();
            mdokter.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };
}
