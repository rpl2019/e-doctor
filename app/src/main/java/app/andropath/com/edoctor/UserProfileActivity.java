package app.andropath.com.edoctor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.*;

public class UserProfileActivity extends AppCompatActivity {
    FirebaseUser firebaseUser;
    String profileid;
    TextView nama, nohp, kota, lahir, jenis, email;
    ImageView Img;
    final String PREF_NIGHT_MODE = "NightMode";

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit_profile:
                Intent intent = new Intent(UserProfileActivity.this, EditProfileActivity.class);
                startActivity(intent);
                userInfo();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.AppTheme);
            SharedPreferences Night = getSharedPreferences(PREF_NIGHT_MODE, Context.MODE_PRIVATE);
            if (Night.getBoolean(PREF_NIGHT_MODE, false)) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        Img = findViewById(R.id.Img);
        nama = findViewById(R.id.txNama);
        nohp = findViewById(R.id.txNohp);
        kota = findViewById(R.id.txKota);
        lahir = findViewById(R.id.txLahir);
        jenis = findViewById(R.id.txJenis);
        email = findViewById(R.id.txEmail);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        SharedPreferences prefs = getSharedPreferences("PREFS", MODE_PRIVATE);
        profileid = prefs.getString("profileid", "none");
        userInfo();
    }

    private void userInfo() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users").child(profileid);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (getApplicationContext() == null){
                   return;
                }

                User user = dataSnapshot.getValue(User.class);
                Glide.with(getApplicationContext()).load(user.getImagePath()).into(Img);
                nama.setText(user.getNama());
                nohp.setText(user.getNohp());
                kota.setText(user.getKota());
                lahir.setText(user.getLahir());
                jenis.setText(user.getJenis());
                email.setText(user.getEmail());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


}
