package app.andropath.com.edoctor;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.firebase.auth.FirebaseAuth;

import static android.content.Context.MODE_PRIVATE;

public class HomeFragment extends Fragment implements View.OnClickListener{

    final String PREF_NIGHT_MODE = "NightMode";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getContext().setTheme(R.style.DarkTheme);
        } else {
            getContext().setTheme(R.style.AppTheme);
            SharedPreferences Night = getContext().getSharedPreferences(PREF_NIGHT_MODE, MODE_PRIVATE);
            if (Night.getBoolean(PREF_NIGHT_MODE, false)) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
        }
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        view.findViewById(R.id.card_view_carirumahsakit).setOnClickListener(this);
        view.findViewById(R.id.card_view_profil).setOnClickListener(this);
        view.findViewById(R.id.card_view_setting).setOnClickListener(this);


            return view;
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.card_view_carirumahsakit) {
            //Do something Like starting an activity
            Intent intent = new Intent(getContext(), CariRS.class);
            startActivity(intent);
        }
        if (v.getId() == R.id.card_view_profil) {
            Bundle intent = getActivity().getIntent().getExtras();
            if (intent != null) {
                String publisher = intent.getString("publisherid");

                SharedPreferences.Editor editor = getContext().getSharedPreferences("PREFS", MODE_PRIVATE).edit();
                editor.putString("profileid", publisher);
                editor.apply();
            }
            //Do something Like starting an activity
            SharedPreferences.Editor editor = getContext().getSharedPreferences("PREFS", MODE_PRIVATE).edit();
            editor.putString("profileid", FirebaseAuth.getInstance().getCurrentUser().getUid());
            editor.apply();
            Intent intent1 = new Intent(getContext(), UserProfileActivity.class);
            startActivity(intent1);
        }
        if (v.getId() == R.id.card_view_setting) {
            //Do something Like starting an activity
            Intent intent = new Intent(getContext(), SettingActivity.class);
            startActivity(intent);
        }
    }

}
