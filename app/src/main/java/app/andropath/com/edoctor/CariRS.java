package app.andropath.com.edoctor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.inputmethod.EditorInfo;
import android.widget.SearchView;

import java.util.ArrayList;

public class CariRS extends AppCompatActivity {
    private RecyclerView recyclerView;
    private AdapterRumahSakit mAdapter;
    private ArrayList<rumahsakit> rumahsakitArrayList;
    private RecyclerView.LayoutManager mLayoutManager;

    final String PREF_NIGHT_MODE = "NightMode";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.AppTheme);
            SharedPreferences Night = getSharedPreferences(PREF_NIGHT_MODE, Context.MODE_PRIVATE);
            if (Night.getBoolean(PREF_NIGHT_MODE, false)) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_rumahsakit);
        createDokterList();
        recyclerView = findViewById(R.id.recycler_view2);
        mLayoutManager = new LinearLayoutManager(CariRS.this);
        mAdapter = new AdapterRumahSakit(rumahsakitArrayList);

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new AdapterRumahSakit.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent (CariRS.this, detailrumahsakit.class);
                intent.putExtra("rumahsakit Item", rumahsakitArrayList.get(position));
                startActivity(intent);
            }
        });

        int gridColumnCount = getResources().getInteger(R.integer.grid_column_count);
        recyclerView.setLayoutManager(new GridLayoutManager(CariRS.this, gridColumnCount));

        SearchView searchView = (SearchView) findViewById(R.id.Search_RS);

        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    public void createDokterList(){
        rumahsakitArrayList = new ArrayList<>();
        rumahsakitArrayList.add(new rumahsakit("Rumah Sakit Al-Islam", "Jl. Soekarno Hatta", "Bandung", R.drawable.rumahsakit));
        rumahsakitArrayList.add(new rumahsakit("Rumah Sakit Santosa", "Jl. Soekarno Hatta", "Bandung", R.drawable.rumahsakit));
        rumahsakitArrayList.add(new rumahsakit("Rumah Sakit Advent", "Jl. Soekarno Hatta", "Bandung", R.drawable.rumahsakit));
        rumahsakitArrayList.add(new rumahsakit("Siloam Hospital", "Jl. Raya Pajajaran No. 27", "Bogor", R.drawable.rumahsakit));
        rumahsakitArrayList.add(new rumahsakit("Rumah Sakit Tebet", "Jl. Lejen M.T Haryono", "Jakarta", R.drawable.rumahsakit));
        rumahsakitArrayList.add(new rumahsakit("Rumah Sakit Umum Yarsi", "Jl. Letjen Soeparto", "Jakarta", R.drawable.rumahsakit));
        rumahsakitArrayList.add(new rumahsakit("Rumah Sakit Permata Jonggol", "Jl. Raya Jonggol", "Bogor", R.drawable.rumahsakit));
    }


}

