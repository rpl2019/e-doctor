package app.andropath.com.edoctor;

public class User {
    private String imagePath;
    private String nama;
    private String nohp;
    private String kota;
    private String lahir;
    private String jenis;
    private String id;
    private String email;

    public User(String id,String imagePath,String nama, String nohp, String kota, String lahir, String jenis, String email) {
        this.id = id;
        this.imagePath = imagePath;
        this.nama = nama;
        this.nohp = nohp;
        this.kota = kota;
        this.lahir = lahir;
        this.jenis = jenis;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User(){}

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNohp() {
        return nohp;
    }

    public void setNohp(String nohp) {
        this.nohp = nohp;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getLahir() {
        return lahir;
    }

    public void setLahir(String lahir) {
        this.lahir = lahir;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

}
