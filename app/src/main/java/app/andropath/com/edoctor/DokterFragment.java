package app.andropath.com.edoctor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.SearchView;

import java.util.ArrayList;

public class DokterFragment extends Fragment {
    private RecyclerView recyclerView;
    private adapterdokter mAdapter;
    private ArrayList<dokter> dokterArrayList;
    private RecyclerView.LayoutManager mLayoutManager;
    final String PREF_NIGHT_MODE = "NightMode";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            getContext().setTheme(R.style.DarkTheme);
        } else {
            getContext().setTheme(R.style.AppTheme);
            SharedPreferences Night = getContext().getSharedPreferences(PREF_NIGHT_MODE, Context.MODE_PRIVATE);
            if (Night.getBoolean(PREF_NIGHT_MODE, false)) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
        }
        View view = inflater.inflate(R.layout.fragment_dokter, container, false);
        createDokterList();
        recyclerView = view.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getContext());
        mAdapter = new adapterdokter(dokterArrayList);

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new adapterdokter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getContext(),detaildokter.class);
                intent.putExtra("dokter Item", dokterArrayList.get(position));
                startActivity(intent);
            }
        });

        int gridColumnCount = getResources().getInteger(R.integer.grid_column_count);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), gridColumnCount));

        SearchView searchView = (SearchView) view.findViewById(R.id.Search_Dokter);

        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return view;
    }

    void createDokterList(){
        dokterArrayList = new ArrayList<>();
        dokterArrayList.add(new dokter("Dimas Maulana", "Jantung", "Bandung",R.drawable.male,"Unpad","e-DOCTOR","123456","Senin-Jumat", "08.00-15.00"));
        dokterArrayList.add(new dokter("Fadly Yonk", "Gigi, Umum, Anak", "Bandung",R.drawable.male,"UI", "Rumah Sakit Al-Islam","9876","Senin-Jumat", "08.00-15.00"));
        dokterArrayList.add(new dokter("Ariyandi Nugraha", "Anak, Gigi", "Bandung",R.drawable.male, "Maranatha","Rumah Sakit Santosa","87654","Senin-Jumat", "08.00-15.00"));
        dokterArrayList.add(new dokter("Monica", "Gizi, Umum", "Bandung",R.drawable.female,"Unpad","Rumah Sakit Pindad","09876","Senin-Jumat", "08.00-15.00"));
        dokterArrayList.add(new dokter("Elysia", "Jantung, Saraf","Jakarta",R.drawable.female,"UI","e-DOCTOR","01234","Senin-Jumat", "08.00-15.00"));
        dokterArrayList.add(new dokter("Ayu", "Pencernaan, Saluran Pernafasan","Jakarta",R.drawable.female,"UNJANI","e-DOCTOR","768990","Senin-Jumat", "08.00-15.00"));
        dokterArrayList.add(new dokter("Santya", "Kulit, Kecantikkan","Jakarta",R.drawable.female,"UNJANI","e-DOCTOR","768990","Senin-Jumat", "08.00-15.00"));

    }


}